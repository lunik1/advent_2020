# Advent of Code 2020

## Usage

To solve the problem on day `n`, run

``` sh
lein run n
```

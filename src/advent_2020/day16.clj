(ns advent-2020.day16
  (:require [clojure.string :as str]
            [clojure.set :as set]))

;;;; Utility functions

(defn- str->keyword
  "Transforms a string into an unqualified keyword, turning whitespace into -"
  [s]
  (keyword (str/replace s #"\s+" "-")))

(defn- transpose
  "The transpose of arr."
  [arr]
  (apply mapv vector arr))

(defn- all?
  "Returns true if all items in `coll` are true, else false."
  [coll]
  (every? identity coll))

;;;; Input parsing

(def ^:private input
  "Raw problem input."
  (io!
   (slurp "resources/day16.txt")))

(def ^:private input-chunks
  "Raw input split into paragraphs with field input, your ticket input, and
  nearby ticket input."
  (zipmap [:fields :your-ticket :nearby-tickets] (str/split input #"\n\n")))

(defn- range-checker
  "For a series of ranges in the form ((min1 max1) (min2 max2)...), returns an
  arity-1 predicate function that checks if the provided argument exists in any
  of the given ranges."
  [ranges]
  (fn [x]
    (boolean (some identity (mapv (fn [[min max]] (<= min x max)) ranges)))))

(defn- parse-field
  "Parse field input `s` into map of field name (keyword) -> predicate function.
  The predicate function returns true if a supplied value exists within the
  ranges specified by field and false otherwise."
  [s]
  (let [field (str->keyword (re-find #"^.+(?=:)" s))
        ranges (->> s
                    (re-seq #"\d+")
                    (map #(Long/parseUnsignedLong %))
                    (partition 2))]
    [field (range-checker ranges)]))

(def ^:private input-fields
  "Map of fields -> predicate functions."
  (->> input-chunks
       :fields
       str/split-lines
       (map parse-field)
       (into {})))

(defn- parse-ticket
  "Parses a comma-separated list of integers `s` into a vector."
  [s]
  (mapv #(Long/parseUnsignedLong %) (str/split s #",")))

(def ^:private your-ticket
  "Values on your ticket."
  (parse-ticket
   (-> input-chunks
       :your-ticket
       str/split-lines
       second)))

(def ^:private nearby-tickets
  "Vector of values on nearby tickets."
  (mapv parse-ticket
        (-> input-chunks
            :nearby-tickets
            str/split-lines
            next)))

(declare any-field?)

(def ^:private nearby-valid-tickets
  "Vector of nearby valid tickets."
  (filter
   (fn [ticket] (all? (map #(any-field? input-fields %) ticket)))
   nearby-tickets))

(def ^:private nearby-values
  "Vector of all nearby values."
  (vec (mapcat identity nearby-tickets)))

;;;; Solution

(defn- any-field?
  "Returns true if `x` is valid for any of the fields specified in `fields`,
  else false. `fields` should be a map of field name -> predicate function."
  [fields x]
  (boolean (some identity (map #(% x) (vals fields)))))

(defn- possible-fields
  "Return the set of possible fields specified by `fields` that could describe
  the values `xs`. `fields` is a map of field name -> predicate function."
  [fields xs]
  (->> fields
       (filter (fn [[field pred?]] (all? (map pred? xs))))
       (map key)
       (apply hash-set)))

(defn- field-order
  "Determines the unique order of `fields` on `tickets`, if it exists. Returns
  nil if there is no unique solution. `fields` is a map of field name ->
  predicate function."
  [fields tickets]
  ;; Algorithm:
  ;; Find all the possible fields for each column stored in vector `possible`.
  ;; Index corresponds to column. Find all the fields which are certain (1
  ;; possibility), put them in the corresponding index in `done`, and remove all
  ;; instances from `possible`. Then loop.  Continue until all fields are in
  ;; `done` (initially filled with nil, so we check for no nils), or there are
  ;; no certain fields, in which case the problem is not uniquely resolvable.
  (loop [done (vec (repeat (count fields) nil))
         possible (map #(possible-fields fields %) (transpose tickets))]
    (let [certain (keep-indexed #(when (= (count %2) 1) [%1 (first %2)]) possible)]
      (cond
        (every? some? done) ; all fields found
        done
        (empty? certain) ; unresolvable
        nil
        :else
        (let [[_ certain-fields] (transpose certain)]
          (recur (apply assoc done (mapcat identity certain))
                 (map #(set/difference % (set certain-fields)) possible)))))))

;;; Entrypoint

(defn run
  []
  (io!
   (println "DAY 16")
   (println "PART 1")
   (println "The ticket scanning error rate is:")
   (println (apply + (remove #(any-field? input-fields %) nearby-values)))
   (println)
   (println "PART 2")
   (println "The field order on the tickets is:")
   (let [order (field-order input-fields nearby-valid-tickets)]
     (println order)
     (println "The product of all departure fields on your tikcet is:")
     (println (as-> your-ticket t
                (zipmap order t)
                (select-keys t [:departure-location
                                :departure-station
                                :departure-platform
                                :departure-track
                                :departure-date
                                :departure-time])
                (vals t)
                (apply * t))))))

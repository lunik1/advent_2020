(ns advent-2020.day13
  (:require [clojure.string :as str]
            [clojure.math.numeric-tower :refer [gcd]]
            [clojure.math.combinatorics :refer [combinations]]))

(def ^:private input
  (slurp "resources/day13.txt"))

(def ^:private notes
  "Bus IDs and start time."
  (let [[start routes] (str/split-lines input)]
    {:start (Integer/parseInt start)
     :routes (->> (str/split routes #",")
                  (remove #(= "x" %))  ; drop out of service routes
                  (map #(Integer/parseInt %)))}))

(def ^:private bus-congs
  "Congruence relations from bus IDs."
  (let [[_ routes] (str/split-lines input)]
    (->> (str/split routes #",")
         vec
         (reduce-kv
          (fn [congs i route]
            (if (= "x" route)
              congs
              (let [route (Integer/parseInt route)]
                (conj congs [(mod (- route i) route) route]))))
          []))))

(defn- first-bus
  "Find the first bus leaving from time start with but IDs routes."
  [{:keys [start routes]}]
  (some
   (fn [time]
     (when-let [route
                (some
                 (fn [route]
                   (when (zero? (rem time route)) route))
                 routes)]
       {:wait (- time start) :route route}))
   (iterate inc start)))

(defn- egcd
  "Returns [x y gcd(x, y)] where ax + by = gcd(a, b)"
  [a b]
  (if (zero? a)
    [0 1 b]
    (let [[x y g] (egcd (mod b a) a)]
      [(- y (* x (quot b a))) x g])))

(defn- modinv
  "Find the modular multiplicative inverse of a mod m. Returns nil if a and m
  are not coprime."
  [a m]
  (let [[x _ g] (egcd a m)]
    (when (= 1 g)
      (mod x m))))

(defn- coprime?
  "Returns true if n and m are coprime, else false."
  [n m]
  (= 1 (gcd n m)))

(defn- pairwise-coprime?
  "Returns true if the set of numbers ns are pairwise coprime, else false."
  [ns]
  (every? (partial apply coprime?) (combinations ns 2)))

;; https://homepages.math.uic.edu/~leon/mcs425-s08/handouts/chinese_remainder.pdf
(defn- congruence-solve-chinese
  "Solve congruence relations congs using the Chinese remainder theorem. A
  congruence relation should be in the form [remainder modulus]."
  [congs]
  (let [[as ms] (apply map vector congs)]
    (assert (pairwise-coprime? ms) "Modulos must be pairwise coprime.")
    (let [Πm (apply * ms)
          zs (map #(/ Πm %) ms)
          ws (map * (map modinv zs ms) zs)]
      (mod (apply + (map * as ws)) Πm))))

(defn run
  []
  (println "DAY 13")
  (println "PART 1")
  (let [{:keys [route wait]} (first-bus notes)]
    (println "The ID of the first bus is:")
    (println route)
    (println "The wait time is:")
    (println wait)
    (println "Their product is:")
    (println (* route wait)))
  (println)
  (println "PART 2")
  (println "The earliest timestamp is:")
  (println (congruence-solve-chinese bus-congs)))

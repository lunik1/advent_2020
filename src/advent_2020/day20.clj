(ns advent-2020.day20
  (:require [clojure.string :as str]
            [clojure.set :refer [difference]]))

(defn- top-edge
  "Get the top edge of tile `t`."
  [t]
  (re-find #".+" t))

(defn- bottom-edge
  "Get the bottom edge of tile `t`."
  [t]
  (re-find #".+$" t))

(defn- left-edge
  "Get the left edge of tile `t`."
  [t]
  (str/join (re-seq #"(?m)^." t)))

(defn- right-edge
  "Get the right edge of tile `t`."
  [t]
  (str/join (re-seq #"(?m).$" t)))

(defn- edges
  "Get all the edges and mirrored edges of `tile` as a set of strings."
  [tile]
  (let [t (top-edge tile)
        b (bottom-edge tile)
        l (left-edge tile)
        r (right-edge tile)]
    (hash-set t (str/reverse t)
              b (str/reverse b)
              l (str/reverse l)
              r (str/reverse r))))

(def ^:private input
  "Raw input, chunked by tile."
  (io!
   (-> "resources/day20.txt"
       slurp
       (str/split #"\n\n"))))

(def ^:private tiles
  "Map of tile id -> tile edges."
  (reduce
   (fn [ts in]
     (let [ms (re-seq #"\d+|(?:#|\.|\n)+" in)]
       (assoc ts
              (-> ms first Long/parseLong)
              (-> ms second edges))))
   {}
   input))

(defn- corners
  "Return the set of ids of corner tiles in `tiles`."
  [tiles]
  ;; Find the tiles with two unmatched edges
  (reduce-kv
   (fn [corners id edges]
     (let [unmatched (apply difference edges (remove #(= edges %) (vals tiles)))]
       (if (= 4 (count unmatched))
         (conj corners id)
         corners)))
   #{}
   tiles))

(defn run
  []
  (io!
   (println "DAY 20")
   (println "PART 1")
   (println "The number of matching messages is")
   (println (apply * (corners tiles)))))

(ns advent-2020.day09
  (:require [clojure.string :as str]
            [clojure.math.combinatorics :as combo]
            [amalloy.ring-buffer :refer [ring-buffer]]))

(def ^:private xmas
  (mapv bigint
        (str/split-lines (slurp "resources/day09.txt"))))

(defn- number-valid?
  "Checks if n is valid.

  n is valid iff exactly two numbers in buf sum to n"
  [buf n]
  ;; TODO: make O(n)
  ;; will need to account for case where two of the same number in buf sum to n
  (some #(when (= (apply + %) n) %)
        (combo/combinations buf 2)))

(defn- find-invalid
  "Finds the first invalid number in xs with buffer size bufsize"
  [bufsize xs]
  (:val
   (let [[pream ys] (split-at bufsize xs)
         ibuf (into (ring-buffer bufsize) pream)]  ; initial buffer
     ;; reduce into structure where :buf key keeps track of the buffer state
     (reduce
      (fn [{:keys [buf]} y]
        (if (number-valid? buf y)
          {:buf (conj buf y) :val nil}
          (reduced {:buf buf :val y})))
      {:buf ibuf :val nil}
      ys))))

(defn- len2-subseqs-from
  "Forms a lazy sequence of all the subsequences in coll starting from index x
  and of at least length 2"
  [x coll]
  (let [vcoll (vec coll)]
    (for [y (range (+ x 2) (-> vcoll count inc))]
      (subvec vcoll x y))))

(defn- find-weakness-seq
  "Finds the contiguous length >1 sequence of number in xs that sum to target"
  [target xs]
  ;; group subsequences by starting position so we can short-circuit if
  ;; the target is exceeded
  (let [grouped-subseqs (map #(len2-subseqs-from % xs) (range 0 (count xs)))]
    (some
     (fn [subseqs]
       (reduce
        (fn [_ subseq]
          (case (compare (apply + subseq) target)
            -1 nil
            0 (reduced subseq)
            1 (reduced nil)  ; all numbers are pos. so give up
            ))
        nil
        subseqs))
     grouped-subseqs)))

(defn- min+max
  "Adds the min and max values in coll"
  [coll]
  (+ (apply min coll) (apply max coll)))

(defn- find-weakness
  "Finds weakness in data xs with predetermined target"
  [target xs]
  (min+max (find-weakness-seq target xs)))

(defn run
  []
  (let [invalid (find-invalid 25 xmas)]
    (println "DAY 9")
    (println "PART 1")
    (println "The first invalid number is:")
    (println (str invalid))
    (println)
    (println "PART 2")
    (println "The encryption weakness is:")
    (println (str (find-weakness invalid xmas)))))

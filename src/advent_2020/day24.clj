(ns advent-2020.day24
  (:require [clojure.string :as str]
            [medley.core :refer [filter-vals]]))

;; Co-ordinate system
;;            _____
;;           /     \   N
;;     _____/  0,1  \_____
;; E  /     \       /     \
;;   / -1,0  \_____/  1,1  \
;;   \       /     \       /
;;    \_____/  0,0  \_____/
;;    /     \       /     \
;;   / -1,-1 \_____/  1,0  \
;;   \       /     \       /
;;    \_____/  0,-1 \_____/  W
;;          \       /
;;       S   \_____/       q,r

(def ^:private input
  (io! (slurp "resources/day24.txt")))

(def ^:private dir->coordinate
  "Map of cardinal directions to change in hexagonal co-ordinates."
  {"nw" [1 1]
   "w" [1 0]
   "sw" [0 -1]
   "se" [-1 -1]
   "e" [-1 0]
   "ne" [0 1]})

(def ^:private flip-locations
  "Locations specified by `input` of tiles to be flipped in hexagonal
  co-ordinates."
  (->> input
       str/split-lines
       (map (comp (partial apply map +)
                  (partial map dir->coordinate)
                  (partial re-seq #"(?:n|s)?(?:e|w)")))))

(defn- moore-hex
  "Return the co-ordinates in the hexagonal moore neighbourhood of `loc` [q r]."
  [loc]
  (mapv #(mapv + loc %) (vals dir->coordinate)))

(defn- evolve
  "Takes a set of the co-ordinates of black tiles `black` and returns a set of
  the co-ordinates of black tiles on the following day."
  [black]
  (set (for [[loc an] (frequencies (mapcat moore-hex black))
             :when (or (#{2} an) (and (#{1} an) (contains? black loc)))]
         loc)))

(defn run
  []
  (let [black (->> flip-locations
                   frequencies
                   (filter-vals odd?)
                   keys
                   set)
        black-100 (-> evolve
                      (iterate black)
                      (nth 100))]
    (io!
     (println "DAY 24")
     (println "PART 1")
     (println "The number of black tiles is:")
     (println (count black))
     (println "PART 2")
     (println "The number of black tiles after 100 days is:")
     (println (count black-100)))))

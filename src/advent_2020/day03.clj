(ns advent-2020.day03
  (:require [clojure.string :as str]))

(def ^:private landscape
  ;; Read landscape into vector of chars (:tiles) and keep original width
  (let [lines (str/split (slurp "resources/day03.txt") #"\n")]
    {:width (count (first lines))
     :tiles (vec (apply str lines))}))

(defn- visited-tiles
  "Create a list of the tile indexes generated with landscape width and slope"
  [width {:keys [right down]}]
  ;; Increment from 0 by (+ right (* down width)). If we reach the edge of the
  ;; landscape - determined by comparing quotients - we effectively lose one row
  ;; of movement, so decrement down in calculation
  (iterate #(+
             right
             (* ((if (= (quot % width) (quot (+ % right) width)) identity dec) down) width)
             %)
           0))

(defn- count-trees
  "Count how many trees are encountered in landscape l by taking trajectory
  specified by step"
  [{:keys [tiles width] :as l} step]
  (count (filter #{\#} (mapv tiles (take-while #(< % (count tiles))
                                               (visited-tiles width step))))))

(defn run
  []
  (do
    (println "DAY 3")
    (println "PART 1")
    (println "The number of trees encountered is:")
    (println (count-trees landscape {:right 3 :down 1}))
    (println)
    (println "PART 2")
    (println "The number of trees encountered on each slope is:")
    (let [ans2 (map (partial count-trees landscape)
                    [{:right 1 :down 1}
                     {:right 3 :down 1}
                     {:right 5 :down 1}
                     {:right 7 :down 1}
                     {:right 1 :down 2}])]
      (println ans2)
      (println "Their product is:")
      (println (apply * ans2)))))

(ns advent-2020.day14
  (:require [clojure.string :as str]
            [clojure.math.combinatorics :refer [selections]]))

(defn- binarize-36
  "Turn n into a vector of \\0s and \\1s representing its 36-bit binary value
  (little endian order)."
  [n]
  (let [chs (Long/toString n 2)
        nchs (count chs)]
    (assert (<= nchs 36) "Value is too large to be represented as a 36-bit unsigned int")
    (vec (concat (repeat (- 36 nchs) \0) chs))))

(defn- debinarize
  "For a sequence of \\0 and \\1s, determine the binary value represented.
  Little endian order"
  [chs]
  (Long/parseUnsignedLong (str/join chs) 2))

(defn- parse-mask-v1
  "Parse a mask for v1 of the program. Returns a map of:
      :andm a mask that a value should be bitwise-and'd with (int)
      :orm a mask that a value should be bitwised-or'd with (int)
  to apply the mask."
  [mstr]
  (->> (re-find #"[01X]+" mstr)
       vec
       (map {\0 [\0 \0] \1 [\1 \1] \X [\1 \0]})
       (apply map vector)
       (map debinarize)
       (zipmap [:andm :orm])))

(defn- parse-mask-v2
  "Parse a mask for v2 of the program. Returns the charachter sequence (\\1s
  \\0s and \\Xs) making up the mask in little endian order."
  [mstr]
  (vec (re-find #"[01X]+" mstr)))

(defn- parse-mem
  "Parse memory address memstr as map of {:adr address :val value}."
  [memstr]
  (->> memstr
       (re-seq #"\d+")
       (map #(Long/parseUnsignedLong %))
       (zipmap [:adr :val])))

(defn- parse-mems
  "Parse a collection of memory adresses memstrs."
  [memstrs]
  (map parse-mem memstrs))

(defn parse-program-common
  "Internal use. Common logic for parsing programs. v1 and v2 only differ by the
  mask parsing function used. Parses prog using maskparse to parse mask."
  [prog maskparse]
  (->> prog
       str/split-lines
       (partition-by #(re-find #"^mask" %))
       (partition 2)
       (map (fn [[mstr memstrs]]
              {:mask (maskparse (first mstr))
               :instructions (parse-mems memstrs)}))))

(defn- parse-program-v1
  "Parse a program for use with version 1 of the decoder chip."
  [prog]
  (parse-program-common prog parse-mask-v1))

(defn- parse-program-v2
  "Parse a program for use with version 2 of the decoder chip."
  [prog]
  (parse-program-common prog parse-mask-v2))

(def ^:private input
  "Raw problem input."
  (slurp "resources/day14.txt"))

(def ^:private program-v1
  (parse-program-v1 input))

(def ^:private program-v2
  (parse-program-v2 input))

(defn- apply-mask
  "Apply a (v1) mask to value v."
  [v {:keys [andm orm] :as msk}]
  (bit-and andm (bit-or orm v)))

(defn- run-progam-v1
  "Run prog for version 1 of the decoder chip."
  [prog]
  (reduce
   (fn [mem {:keys [mask instructions]}]
     (reduce
      (fn [mem {:keys [adr val]}]
        (assoc mem adr (apply-mask val mask)))
      mem
      instructions))
   {}
   prog))

(defn- apply-floating-mask
  "Apply mask msk with floating bits (\\X) to v. v and msk should be sequences of
  \\0, \\1, or \\X in little endian order and the same length. Returns a vector."
  [v msk]
  (mapv #({\0 %1 \1 \1 \X \X} %2) v msk))

(defn- explode-floating-bits
  "For binary value with floating bits (\\X) return a lazy sequence of all the
  possible values it could take. bin should be a vector of \\0, \\1, or \\X in
  little endian order."
  [bin]
  (let [floatidx (->> bin
                      (map-indexed vector)
                      (filter (fn [[_ ch]] (= ch \X)))
                      (map first))]
    (map
     (fn [bits]
       (debinarize (apply assoc bin (interleave floatidx bits))))
     (selections [\0 \1] (count floatidx)))))

(defn- run-program-v2
  "Run prog for version 2 of the decoder chip."
  [prog]
  (reduce
   (fn [mem {:keys [mask instructions]}]
     (reduce
      (fn [mem {:keys [adr val]}]
        (apply assoc mem (interleave (explode-floating-bits (apply-floating-mask (binarize-36 adr) mask))
                                     (repeat val))))
      mem
      instructions))
   {}
   prog))

(defn- sum-memory
  "Sum all the values in program memory mem. mem is a map of address -> value."
  [mem]
  (apply + (vals mem)))

(defn run
  []
  (println "DAY 14")
  (println "PART 1")
  (println "The sum of all the values in memory is:")
  (println (sum-memory (run-progam-v1 program-v1)))
  (println)
  (println "PART 2")
  (println "The sum of all the values in memory is:")
  (println (sum-memory (run-program-v2 program-v2))))

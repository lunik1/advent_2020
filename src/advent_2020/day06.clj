(ns advent-2020.day06
  (:require [clojure.set :as set]
            [clojure.string :as str]))

(def ^:private answers
  (as-> "resources/day06.txt" $
    (slurp $)
    (str/split $ #"\n\n")  ; groups are separated by a blank line
    (map str/split-lines $)))

(defn- unionise
  "clojure.set/union for a coll of colls"
  [colls]
  (transduce (map set) set/union colls))

(defn- intersectionise
  "clojure.set/intersection for a coll of colls"
  [colls]
  (apply set/intersection (map set colls)))

(defn- recount
  "Recursive count: count the total number of elements in a coll of colls"
  [coll]
  (transduce (map count) + coll))

(defn run
  []
  (do
    (println "DAY 6")
    (println "PART 1")
    (println "The number of questions to which anyone answered \"yes\" is:")
    (println (recount (map unionise answers)))
    (println)
    (println "PART 2")
    (println "The number of questions to which anyone answered \"yes\" is:")
    (prn (recount (map intersectionise answers)))))

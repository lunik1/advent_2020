(ns advent-2020.day05
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]))

(def ^:private passes
  (str/split-lines (slurp "resources/day05.txt")))

(defn- debinarize
  "For a sequence of \\0 and \\1s, determine the binary value represented.
  Little endian order"
  [chs]
  (Integer/parseInt (str/join chs) 2))

(defn- get-row
  "Get the row number from \"FBFBF...\" specification"
  [rs]
  (debinarize (map {\F \0 \B \1} rs)))

(defn- get-column
  "Get the column number from \"LR...\" specification"
  [rs]
  (debinarize (map {\L \0 \R \1} rs)))

(defn- get-seat
  "Get seat and row number from binary space partitioning given on boarding
  pass"
  [s]
  (let [[row-spec column-spec] (split-at 7 s)]  ; 7 F|Bs, 3 L|Rs
    {:row (get-row row-spec) :column (get-column column-spec)}))

(defn- seat-id
  "Calculate the seat ID"
  [{:keys [row column]}]
  (+ column (* row 8)))

(defn- find-seat
  "Finds the missing seat in a sorted list of ids"
  [ids]
  ;; Find where two adjacent values are 2 apart, and return the number between
  (some (fn [[x y]] (when (= (+ x 2) y) (inc x))) (partition 2 1 ids)))

(defn run
  []
  (let [seat-ids (->> passes (map (comp seat-id get-seat)) sort vec)]
    (println "DAY 5")
    (println "PART 1")
    (println "The greatest seat ID is:")
    (println (peek seat-ids))
    (println)
    (println "PART 2")
    (println "Your seat ID is:")
    (println (find-seat seat-ids))))

(ns advent-2020.day25
  (:require [clojure.string :as str]
            [medley.core :refer [indexed]]))

(def card-public-key 16050997)

(def door-public-key 8458505)

(defn- rounds
  [sn]
  (reductions
   (fn [v _]
     (rem (* v sn) 20201227))
   1
   (range)))

(defn- transform
  [sn ls]
  (nth (rounds sn) ls))

(defn- loop-size
  [sn key]
  (->> sn
       rounds
       indexed
       (some (fn [[i v]] (when (#{key} v) i)))))

(defn run
  []
  (let [card-loop-size (loop-size 7 card-public-key)
        door-loop-size (loop-size 7 door-public-key)
        card-encryption-key (transform card-public-key door-loop-size)
        door-encryption-key (transform door-public-key card-loop-size)]
    (assert (= door-encryption-key card-encryption-key))
    (io!
     (println "DAY 25")
     (println "PART 1")
     (println "The encryption key is")
     (println card-encryption-key))))

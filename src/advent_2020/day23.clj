(ns advent-2020.day23
  (:require [clojure.string :as str]
            [medley.core :refer [find-first indexed map-entry]]))

(def ^:private crab-cups [4 6 3 5 2 8 1 7 9])

(defn- more-crab-cups
  "Add sequentially numbered cups to `crab-cups` until there are 1000000 cups."
  []
  (concat crab-cups
          (range (inc (apply max crab-cups)) 1000001)))

(defn- cups->map
  "Transfrom the order of cups into a map of label -> next label."
  [cups]
  (->> cups
       cycle
       (partition 2 1)
       (take (count cups))
       (map (partial apply map-entry))
       (into {})))

(defn- cup-order
  "Retrieve a lazy seq of the cup order staring from label `n` from `cupmap`.
  `n` defaults to 1 if not given."
  ([cupmap] (cup-order cupmap 1))
  ([cupmap n]
   (take (count cupmap) (iterate cupmap n))))

(defn- destination
  "With current cup label `curr` and cups remaining after pick-up `rcups`, find
  the label of the destination cup."
  [curr rcups]
  ;; Not pretty, but fast.
  (cond
    (contains? rcups (dec curr))
    (dec curr)
    (contains? rcups (- curr 2))
    (- curr 2)
    (contains? rcups (- curr 3))
    (- curr 3)
    (contains? rcups (- curr 4))
    (- curr 4)
    :else
    (apply max (keys rcups))))

(defn- move-cups
  "Perform one round of moving cups starting from `state`, returning the new
  state."
  [state]
  (let [{:keys [:cupmap :curr]} state
        pick (vec (take 3 (rest (cup-order cupmap curr))))
        d (destination curr (apply dissoc cupmap pick))]
    (-> state
        (assoc-in [:cupmap d] (pick 0))
        (assoc-in [:cupmap (peek pick)] (cupmap d))
        (assoc-in [:cupmap curr] (cupmap (peek pick)))
        (#(assoc % :curr ((:cupmap %) curr))))))

(defn- moves
  "Return a lazy sequence of game states with initial cup order described by
  `cupmap`, and initial current cup label `n`. A game state is a map of
      :cupmap -> map describing cup order
      :curr -> label of current cup."
  [cupmap n]
  (iterate move-cups {:cupmap cupmap :curr n}))

(defn run
  []
  (io!
   (println "DAY 23")
   (println "PART 1")
   (println "The labels on the cups after 1 is:")
   (println (-> crab-cups
                cups->map
                (moves (first crab-cups))
                (nth 100)
                :cupmap
                cup-order
                rest
                str/join))
   (println "PART 2")
   (println "The product of the two cup labels after cup 1 is:")
   (println (->> (-> (more-crab-cups)
                     cups->map
                     (moves (first crab-cups))
                     (nth 10000000)
                     :cupmap
                     cup-order
                     rest)
                 (take 2)
                 (apply *)))))

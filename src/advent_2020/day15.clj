(ns advent-2020.day15
  (:require [clojure.string :as  str]))

(def ^:private start [1 20 8 12 0 14])

(defn- van-eck
  "Return a (lazy) Van Eck sequence starting with seed."
  [seed]
  (let [s (first seed)
        fsi (dec (count seed))] ; final seed index
    (map :val
         (reductions
          (fn [{:keys [val mem] :as state} i]
            (let [last-seen (mem val)
                  nxt (cond
                        (<= i fsi) (nth seed i)
                        (nil? last-seen) 0
                        :else (- i last-seen))]
              (-> state
                  (assoc :val nxt)
                  (update :mem #(assoc % val i)))))
          {:mem {s 0} :val s}
          (iterate inc 1)))))

(defn run
  []
  (println "DAY 15")
  (println "PART 1")
  (println "The 2020th number is:")
  (println (nth (van-eck start) 2019))
  (println)
  (println "PART 2")
  (println "The 30000000th number is:")
  (println (nth (van-eck start) 29999999)))

(ns advent-2020.day21
  (:require [clojure.string :as str]
            [clojure.set :as set]))

;;;; Input parsing

(def ^:private foods
  "Vector of foods. Each food is a map of
      :ingredients -> set of ingredients
      :allergens -> set of allergens"
  (->> "resources/day21.txt"
       slurp
       str/split-lines
       (map (comp (partial map (comp (partial apply hash-set)
                                     (partial map keyword)
                                     #(str/split % #"(?:,|\s)+")))
                  #(str/split % #" \(contains ")
                  str/join
                  drop-last))
       (map #(zipmap [:ingredients :allergens] %))
       vec)) ; what a mess

;;;; Solution

(defn- allergens
  "All the allergens referred to in `foods`."
  [foods]
  (->> foods
       (map :allergens)
       (apply set/union)))

(defn- ingredients
  "All the ingredients referred to in `foods`."
  [foods]
  (->> foods
       (map :ingredients)
       (apply set/union)))

(defn- allergen-possibilities
  "For the provided `foods`, return a map of allergens -> a set of ingredients,
  where every ingredient in the set always appears alongside the allergen."
  [foods]
  (reduce
   (fn [m allergen]
     (assoc m allergen
            (apply set/intersection (->> foods
                                         (filter #((:allergens %) allergen))
                                         (map :ingredients)))))
   {}
   (allergens foods)))

(defn- allergen-ingredients
  "Determines the unique mapping of allergens -> ingredients in `foods`. Returns
  nil if one does not exist."
  [foods]
  ;; Adapted from algorithm used on day 16
  (loop [done {}
         possible (allergen-possibilities foods)]
    (let [certain (into {} (keep (fn [[allgn ingrs]]
                                   (when (= (count ingrs) 1) [allgn (first ingrs)]))
                                 possible))]
      (cond
        (= (count possible) (count done))
        done
        (empty? certain)
        nil
        :else
        (recur (into done certain)
               (reduce #(update %1 %2 set/difference (set (vals certain)))
                       possible
                       (keys possible)))))))

(defn- appearances
  "Return the number of times `v` appears in a collection of sets, `sets`."
  [v sets]
  (count (keep #(% v) sets)))

(defn- canonical-dangerous-ingredient-list
  "Form the canonical dangerous ingredients list from a mapping of allergens ->
  ingredietns `ai` obtained by [[allergen-ingredients]]."
  [ai]
  (->> ai
       (into (sorted-map))
       vals
       (map name)
       (str/join ",")))

;;;; Entrypoint

(defn run
  []
  (let [ai (allergen-ingredients foods)
        nai (set/difference (ingredients foods) (vals ai))]
    (io!
     (println "DAY 21")
     (println "PART 1")
     (println "The number of times non-allergenic ingredients appear is:")
     (println (apply + (map #(appearances % (map :ingredients foods)) nai)))
     (println)
     (println "PART 2")
     (println "The canonical dangerous ingredient list is:")
     (println (canonical-dangerous-ingredient-list ai)))))

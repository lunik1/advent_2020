(ns advent-2020.day07
  (:require [clojure.string :as str]
            [loom.graph :as graph]
            [loom.alg-generic :as lag]))

(def ^:private rules
  (str/split-lines (slurp "resources/day07.txt")))

(defn- str->keyword
  "Transforms a string into an unqualified keyword, turning whitespace into -"
  [s]
  (keyword (str/replace s #"\s+" "-")))

(defn- parse-rule
  "Parse a customs rule"
  [r]
  (let [bags (re-seq #"\w+\s+\w+(?=\s+bag)" r) ; bags are named <word> <word> bag(s)
        parent (str->keyword (first bags))  ; first bag is parent
        children (map str->keyword (rest bags))  ; subsequent bags are children
        weights (map #(Integer/parseInt %) (re-seq #"\d+" r))]  ; no.s are weights, position corr. to children
    (map vector (repeat parent) children weights)))  ; [parent child weight] can be consumed by loom

(defn- parse-rules
  "Parse customs rules from list of strings rs"
  [rs]
  (transduce (map parse-rule) into rs))

(defn- graph-rules
  "Transform parsed rules into a weighted digraph"
  []
  (apply graph/weighted-digraph (parse-rules rules)))

(defn- can-contain?
  "True if bag b1 can, eventually, contain bag b2. Else, false.

  Requires rules in the form of a (weighted) digraph, wdg."
  [wdg b1 b2]
  (if (= b1 b2)  ; trivial path, false as bag does not contain itself
    false
    ;; Find if a path exists from b1 to b2
    (boolean
     (lag/bf-path-bi #(graph/successors wdg %)
                     #(graph/predecessors wdg %)
                     b1
                     b2))))

(defn- empty-bags
  "Counts the number of empty bags in bag (non-recursively) using rules
  represented by wdg."
  [wdg bag]
  (transduce
   (comp
    (map (partial graph/weight wdg bag))
    (filter pos?))
   +
   (graph/successors wdg bag)))

(def ^:private n-bags-contained
  "Counts the number of empty bags in bag (recursively) using rules
  represented by wdg."
  (memoize
   (fn [wdg bag]
     ;; The number of bags in a bag b is:
     ;; (no. of empty bags in b) +
     ;; (for each bag child in b: (no. of bags in child) * (no. of child in b))
     (let [inbag (graph/successors wdg bag)]
       (if (empty? inbag)
         0  ; base case - a bag with no successors is empty
         (transduce
          (map #(* (graph/weight wdg bag %) (n-bags-contained wdg %)))
          +
          (empty-bags wdg bag)
          inbag))))))

(defn run
  []
  (let [rules-graph (graph-rules)]
    (println "DAY 7")
    (println "PART 1")
    (println "The number of bags that can contain a shiny gold bag is:")
    (println (count (filter #(can-contain? rules-graph % :shiny-gold) (graph/nodes rules-graph))))
    (println)
    (println "PART 2")
    (println "The number of bags contained in a shiny gold bag is:")
    (println (n-bags-contained rules-graph :shiny-gold))))

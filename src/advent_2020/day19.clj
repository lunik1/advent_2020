(ns advent-2020.day19
  (:require [clojure.string :as str]
            [instaparse.core :as insta]))

(defn- order-rules
  "Sort the lines in s alphabetically."
  [s]
  ;; This ensures rule 0 is first so instaparse will use it as the starting rule
  (->> s
       str/split-lines
       sort
       (str/join "\n")))

(def ^:private input
  (update
   (zipmap [:rules :messages]
           (io!
            (-> "resources/day19.txt"
                slurp
                (str/split #"\n\n"))))
   :rules
   order-rules))

(def ^:private messages
  (-> input
      :messages
      str/split-lines))

(def ^:private parser-1
  (-> input
      :rules
      insta/parser))

(defn- change-rules
  "Change rules 8 and 11 for part 2 of the problem."
  [s]
  (-> s
      (str/replace-first #"8: 42" "8: 42 | 42 8")
      (str/replace-first #"11: 42 31" "11: 42 31 | 42 11 31")))

(def ^:private parser-2
  (-> input
      :rules
      change-rules
      insta/parser))

(defn run
  []
  (io!
   (println "DAY 17")
   (println "PART 1")
   (println "The number of matching messages is")
   (println (count (remove (comp insta/failure? parser-1) messages)))
   (println)
   (println "PART 2")
   (println "The number of matching messages is")
   (println (count (remove (comp insta/failure? parser-2) messages)))))

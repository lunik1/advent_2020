(ns advent-2020.day18
  (:require [clojure.string :as str]))

;;;; Input parsing

(def ^:private homework
  (io!
   (->> "resources/day18.txt"
        slurp
        str/split-lines)))

;;;; Solution

(defn- match-paren-dist
  "Returns the distance to the matching ) in `tokens`."
  [tokens]
  (reduce
   (fn [{:keys [dist level] :as state} str]
     (case str
       "(" (-> state (update :dist inc) (update :level inc))
       ")" (if (zero? level)
             (reduced dist)
             (-> state (update :dist inc) (update :level dec)))
       (update state :dist inc)))
   {:dist 1 :level 0}
   tokens))

(defn- calculate-impl
  "For internal use by `calculate`. Iterates through token stack, recursing
  when encountering a sub-expression."
  [stack]
  (loop [stack stack
         op +
         tot 0]
    (if (seq stack)
      (let [token (first stack)]
        (condp re-matches token
          ;; Start of sub-expression, skip the outer stack to after the matching
          ;; ")" and recur with rest as inner stack
          #"\("
          (recur (nthrest stack (-> stack rest match-paren-dist inc))
                 op
                 (op tot (calculate-impl (rest stack))))

          ;; End of sub-expression: return
          #"\)"
          tot

          ;; Number: combine with current total using last seen operator
          #"\d+"
          (recur (rest stack)
                 op
                 (op tot (Long/parseUnsignedLong token)))

          ;; Operator: update last-seen operator
          #"\+|\*"
          (recur (rest stack)
                 ({"+" + "*" *} token)
                 tot)))
      tot)))

(defn- calculate
  "Takes a mathematical expression `s` in infix notation expression as a string
  and returns the result. The only allowed operators are +, *, and parentheses.
  +/* precedence is determined b order encountered leading left to right, not
  BODMAS."
  [s]
  (calculate-impl (re-seq #"\(|\)|\d+|\+|\*" s)))

(defn- reprioritise
  "Mangles an expression `s` such that + binds more strongly than *."
  [s]
  (-> s
      (str/replace #"\(" "(((")
      (str/replace #"\)" ")))")
      (str/replace #"^" "(((")
      (str/replace #"$" ")))")
      (str/replace #" \* " ")) * ((")
      (str/replace #" \+ " ") + (")))

;;;; Entrypoint

(defn run
  []
  (io!
   (println "DAY 18")
   (println "PART 1")
   (println "The total of all homework answers is:")
   (println (apply + (map calculate homework)))
   (println)
   (println "PART 1")
   (println "The total of all homework answers is:")
   (println (apply + (map (comp calculate reprioritise) homework)))))

(ns advent-2020.day04
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as str]))

;; birth year between 1920 and 2002
(s/def ::byr (s/and int? #(<= 1920 % 2002)))

;; issue year between 2010 and 2020
(s/def ::iyr (s/and int? #(<= 2010 % 2020)))

;; expiration year between 2020 and 2030
(s/def ::eyr (s/and int? #(<= 2020 % 2030)))

;; height value should be an int
(s/def ::val int?)

;; height unit should be :cm or :in
(s/def ::unit #{:cm :in})

;; height :val between 59 and 76 inches or 150 and 193 cm
(s/def ::hgt (s/and (s/keys ::req-un [::val ::unit])
                    #(case (:unit %)
                       :in (<= 59 (:val %) 76)
                       :cm (<= 150 (:val %) 193)
                       false)))

;; hair colour should be a valid rgb hex triplet
(s/def ::hcl (s/and string? #(re-matches #"#[\da-f]{6}" %)))

;; eye colour should be one of the allowed values
(s/def ::ecl #{:amb :blu :brn :gry :grn :hzl :oth})

;; passport id should be 9 digits
(s/def ::pid (s/and string? #(re-matches #"\d{9}" %)))

;; passport must contain the specified keys (and must conform to their own spec!)
(s/def ::passport (s/keys :req-un [::byr ::iyr ::eyr ::hgt ::hcl ::ecl ::pid]
                          :opt-un [::cid]))

(defn- str->height
  "Convert a string containing height data to a map"
  [s]
  (->> s
       (re-matches #"(\d+)(\D+)")
       rest
       (map #(%1 %2) [#(Integer/parseInt %) keyword])
       (zipmap [:val :unit])))

(defn- coerce-passport-field
  "Coerce the values of a passport from strings"
  [k v]
  (condp contains? k
    #{"byr" "iyr" "eyr"} (Integer/parseInt v)  ; years as ints
    #{"ecl"} (keyword v)  ; eye colour as keyword
    #{"hgt"} (str->height v)  ; height as map
    v))  ; keep the rest as strings

(defn- str->passport
  "Convert a string containing passport data to a map"
  [s]
  (transduce
   (map #(str/split % #":"))  ; "key:val" -> ["key" "val"]
   (completing (fn [m me]
                 (try  ; skip this entry if anything goes wrong
                   (assoc m (keyword (me 0)) (apply coerce-passport-field me))
                   (catch Exception e m))))
   {}
   (str/split s #"\s+")))  ; "key1:val1 key2:val2" -> ["key1:val1" "key2:val2"]

(def ^:private passports
  ;; Read in passports data from file
  (as-> "resources/day04.txt" $
    (slurp $)
    (str/split $ #"\n\n")  ; passports are separated by a blank line
    (map str->passport $)))

(defn- passport-has-keys?
  "Checks if passport p contains the expected keys"
  [p]
  (every? identity (map contains? (repeat p) [:byr :iyr :eyr :hgt :hcl :ecl :pid])))

(defn- passport-valid?
  "Checks if passport p confirms to the passport spec"
  [p]
  (s/valid? ::passport p))

(defn run
  []
  (do
    (println "DAY 4")
    (println "PART 1")
    (println "The number valid passports is:")
    (println (count (filter passport-has-keys? passports)))
    (println)
    (println "PART 2")
    (println "The number valid passports is:")
    (println (count (filter passport-valid? passports)))))

(ns advent-2020.day22
  (:require [clojure.string :as str]
            [medley.core :refer [queue find-first]]))

;;;; Input parsing

(def ^:private input
  (io! (slurp "resources/day22.txt")))

(def ^:private player-1-deck
  (map #(Long/parseUnsignedLong %) (-> input
                                       (str/split #"\n\n")
                                       first
                                       str/split-lines
                                       next)))

(def ^:private player-2-deck
  (map #(Long/parseUnsignedLong %) (-> input
                                       (str/split #"\n\n")
                                       second
                                       str/split-lines
                                       next)))

;;;; Game implementation

(defn- winning-deck
  "Returns the final state of the winning deck after non-recursive combat.
  `deck1` is Player 1's deck and `deck2` is Player 2's deck."
  [deck1 deck2]
  (loop [d1 (queue deck1)
         d2 (queue deck2)]
    (if (every? seq [d1 d2])
      (let [c1 (peek d1)
            c2 (peek d2)]
        (if (> c1 c2)
          (recur (conj (pop d1) c1 c2) (pop d2))
          (recur (pop d1) (conj (pop d2) c2 c1))))
      (find-first seq [d1 d2]))))

(defn- recursive-combat
  "Perform a game of recursive combat with `deck1` as Player 1's deck and
  `deck2` as Player 2's deck. Returns a map of
      :win -> # player that won the game
      :deck -> final state of winner's deck."
  [deck1 deck2]
  (loop [d1 (queue deck1)
         d2 (queue deck2)
         prev (transient #{})]
    (cond
      ;; Game ends due to empty deck
      (empty? d1)
      {:win 2 :deck d2}

      (empty? d2)
      {:win 1 :deck d1}

      ;; Prevent loop
      (contains? prev [d1 d2])
      {:win 1 :deck d1}

      :else
      (let [prev (conj! prev [d1 d2])
            c1 (peek d1)
            c2 (peek d2)
            d1 (pop d1)
            d2 (pop d2)]
        (cond
          ;; Recursive round
          (and (>= (count d1) c1) (>= (count d2) c2))
          (case (:win (recursive-combat (take c1 d1) (take c2 d2)))
            1 (recur (conj d1 c1 c2) d2 prev)
            2 (recur d1 (conj d2 c2 c1) prev))

          ;; Non-recursive round; higher card wins
          (> c1 c2)
          (recur (conj d1 c1 c2) d2 prev)

          :else
          (recur d1 (conj d2 c2 c1) prev))))))

(defn- score
  "Returns the sum of multipling the last item in `deck` by 1, the penultimate
  by 2, the thrid-from-last by 3, etc."
  [deck]
  (apply + (map * (range (count deck) 0 -1) deck)))

(defn run
  []
  (io!
   (println "DAY 22")
   (println "PART 1")
   (println "The score of the winning player is:")
   (println (score (winning-deck player-1-deck player-2-deck)))
   (println)
   (println "PART 2")
   (println "The score of the winning player is:")
   (println (->> player-2-deck
                 (recursive-combat player-1-deck)
                 :deck
                 score))))

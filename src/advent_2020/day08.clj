(ns advent-2020.day08
  (:require [clojure.string :as str]))

(defn- parse-instruction
  "Parses an instruction into a map of:
      :op - the requested operation
      :arg - the provided argument"
  [ins]
  (let [[op arg] (str/split ins #"\s+")]
    {:op (keyword op)
     :arg (Integer/parseInt arg)}))

(defn- parse-program
  "Parses the instructions into a program"
  [instructions]
  (mapv parse-instruction instructions))

(def ^:private program
  (parse-program (str/split-lines (slurp "resources/day08.txt"))))

(defn- run-prog
  "Runs program prog, stops when attempting to run the instruction immediately
  following the last, or when a loop is encountered. Returns a map of:
      :term - true if the program terminated, false if it looped
      :acc - final value of accumulator"
  [prog]
  (loop [idx 0
         acc 0
         seen #{}]
    (cond
      (contains? seen idx)  ; looped
      {:term false :acc acc}

      (= idx (count prog))  ; terminated
      {:term true :acc acc}

      :else
      (let [{:keys [op arg]} (prog idx)]
        (case op
          :nop (recur (inc idx) acc (conj seen idx))
          :acc (recur (inc idx) (+ acc arg) (conj seen idx))
          :jmp (recur (+ idx arg) acc (conj seen idx))
          (throw (Exception. (format "Unknown operation %s" op))))))))

(defn- replace-op
  "Replaces the operation at index n of prog with op"
  [prog n op]
  (assoc-in prog [n :op] op))

(defn- fix-prog-dumb
  "Searches for an instance of :nop that can be replaced with :jmp or vice versa
  that causes prog to terminate. Returns the final value of the accumulator for
  the terminated program."
  ;; TODO: do this the smart way and only change instructions that are actually
  ;; encountered during program execution, or possibly by working backwards from
  ;; the end state
  [prog]
  (some
   (fn [[idx {:keys [op]}]]
     (case op
       :acc nil
       :nop (let [{:keys [term acc]} (run-prog (replace-op prog idx :jmp))]
              (when term acc))
       :jmp (let [{:keys [term acc]} (run-prog (replace-op prog idx :nop))]
              (when term acc))
       (throw (Exception. (format "Unknown operation %s" op)))))
   (map-indexed vector prog)))

(defn run
  []
  (do
    (println "DAY 8")
    (println "PART 1")
    (println "The value of the accumulator is:")
    (println (:acc (run-prog program)))
    (println)
    (println "PART 2")
    (println "The value of the accumulator is:")
    (println (fix-prog-dumb program))))

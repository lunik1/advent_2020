(ns advent-2020.core
  (:gen-class)
  (:require [advent-2020.day01 :as day01]
            [advent-2020.day02 :as day02]
            [advent-2020.day03 :as day03]
            [advent-2020.day04 :as day04]
            [advent-2020.day05 :as day05]
            [advent-2020.day06 :as day06]
            [advent-2020.day07 :as day07]
            [advent-2020.day08 :as day08]
            [advent-2020.day09 :as day09]
            [advent-2020.day10 :as day10]
            [advent-2020.day11 :as day11]
            [advent-2020.day12 :as day12]
            [advent-2020.day13 :as day13]
            [advent-2020.day14 :as day14]
            [advent-2020.day15 :as day15]
            [advent-2020.day16 :as day16]
            [advent-2020.day17 :as day17]
            [advent-2020.day18 :as day18]
            [advent-2020.day19 :as day19]
            [advent-2020.day20 :as day20]
            [advent-2020.day21 :as day21]
            [advent-2020.day22 :as day22]
            [advent-2020.day23 :as day23]
            [advent-2020.day24 :as day24]
            [advent-2020.day25 :as day25]))

(defn- solution-dispatch
  "Solve the requested problem"
  [x]
  (case x
    1 (day01/run)
    2 (day02/run)
    3 (day03/run)
    4 (day04/run)
    5 (day05/run)
    6 (day06/run)
    7 (day07/run)
    8 (day08/run)
    9 (day09/run)
    10 (day10/run)
    11 (day11/run)
    12 (day12/run)
    13 (day13/run)
    14 (day14/run)
    15 (day15/run)
    16 (day16/run)
    17 (day17/run)
    18 (day18/run)
    19 (day19/run)
    20 (day20/run)
    21 (day21/run)
    22 (day22/run)
    23 (day23/run)
    24 (day24/run)
    25 (day25/run)
    (println "Not a valid day!")))

(defn -main
  [& args]
  (solution-dispatch (Integer/parseInt (first args))))

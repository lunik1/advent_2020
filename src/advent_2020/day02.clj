(ns advent-2020.day02
  (:require [clojure.string :as str]))

(def ^:private policies
  ;; Read in data and convert to list of maps with keys:
  ;;     :n1 :: first value speifying policy
  ;;     :n2 :: second value specifying policy
  ;;     :ch :: char of interest in policy
  ;;     :pass :: password subject to policy
  (map
   (fn [lines] (zipmap
                [:n1 :n2 :ch :pass]
                (map #(%1 %2)
                     [#(Integer/parseInt %)
                      #(Integer/parseInt %)
                      first  ; str to char
                      identity]
                     (rest (re-matches #"(\d+)-(\d+) (.): (.+)" lines)))))
   (str/split (slurp "resources/day02.txt") #"\n")))

(defn- valid-old-policy?
  "Check if a policy is valid

  In this case, n1 and n2 represent the min and max allowed occurrences of ch"
  [{:keys [n1 n2 ch pass]}]
  (let [nch (get (frequencies pass) ch 0)]
    (<= n1 nch n2)))

(defn- valid-new-policy?
  "Check if a policy is valid

  In this case, n1 and n2 represent 1-indexed indices in pass. The policy is
  valid iff n1 xor n1 contain ch"
  [{:keys [n1 n2 ch pass]}]
  (not= (= (nth pass (dec n1)) ch) (= (nth pass (dec n2)) ch)))

(defn run
  []
  (do
    (println "DAY 2")
    (println "PART 1")
    (println "The number of valid policies is:")
    (println (count (filter valid-old-policy? policies)))
    (println)
    (println "PART 2")
    (println "The number of valid policies is:")
    (println (count (filter valid-new-policy? policies)))))

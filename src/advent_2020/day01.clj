(ns advent-2020.day01
  (:require [clojure.string :as str]
            [clojure.math.combinatorics :as combo]))

(def ^:private expenses
  (map
   #(Integer/parseInt %)
   (str/split (slurp "resources/day01.txt") #"\n")))

(defn- find-entries
  "Find the n entries in xs that sum to target"
  [n target xs]
  (some #(let [compl (- target (apply + %))]
           (when (contains? (set xs) compl) (conj % compl)))
        (combo/combinations xs (dec n))))

(defn run
  "Solve this problem"
  []
  (let [ans1 (find-entries 2 2020 expenses)
        ans2 (find-entries 3 2020 expenses)]
    (println "DAY 1")
    (println "PART 1")
    (println "The two entries that sum to 2020 are:")
    (println ans1)
    (println "Their product is:")
    (println (apply * ans1))
    (println)
    (println "PART 2")
    (println "The three entries that sum to 2020 are:")
    (println ans2)
    (println "Their product is:")
    (println (apply * ans2))))

(ns advent-2020.day11
  (:require [clojure.string :as str]))

(defn- mapv-indexed
  "Map with indexes, like map-indexed, but return a vector, like mapv."
  [f coll]
  (vec (map-indexed f coll)))

(defn- equilibrium
  "Iterate through x, (f x), (f (f x)) etc. until applying f has no effect.
  Return that result."
  [f x]
  (->> x
       (iterate f)
       (partition 2 1)
       (some #(when (apply = %) (first %)))))

(defn- around
  "For co-oridnate [x j] return a lazy seq of the co-ordinates around [x y] to
  the N, S, E, W, NW, NE, SE, and SW. E.g. for [1, 0]:
      ([0 -1] [0 0] [0 1] [1 -1] [1 1] [2 -1] [2 0] [2 1])"
  [[x y]]
  (for [i (range (dec x) (+ x 2))
        j (range (dec y) (+ y 2))
        :when (not (and (= x i) (= y j)))]
    [i j]))

(defn- gen-path
  "Returns a lazy seq of
  [x, y], [(+ x incx), (+ y incy)], [(+ incx (+ x incx)), (+ incy (+ y incy))]…"
  [x y incx incy]
  (iterate (fn [[x y]] [(+ incx x) (+ incy y)]) [x y]))

(defn- neighbours
  "Return all the neighbours of the element in arr at [i j] (8 directions)"
  [i j arr]
  (let [locs (around [i j])]
    (reduce
     (fn [nbs loc] (if-let [neighbour (get-in arr loc)]
                     (conj nbs neighbour)
                     nbs))
     []
     locs)))

(defn- first-seat
  "Find the first seat (\\# or \\L) in tiles. Returns nil if no seat is found."
  [tiles]
  (some #(when (contains? #{\# \L} %) %) tiles))

(defn- seeable-seats
  "Return the seats that can be seen from position [i j] in layout lyt."
  [i j lyt]
  (map
   (fn [[x y]]
     (first-seat (map #(get-in lyt % \L) (rest (gen-path i j x y)))))
   (around [0 0])))

(defn- count-occupied
  "Count the number of occupied (\\#) seats the possibly nested collection of
  tiles lyt"
  [lyt]
  (->> lyt
       flatten
       (filter #{\#})
       count))

(defn- update-tile-1
  "Tile update function for part 1 of the problem: a seat is filled if no
  neighbouring seats are occupied, and emptied if four or more neighbouring
  seats are occupied."
  [i j lyt]
  (let [t (get-in lyt [i j])
        adj (neighbours i j lyt)]
    (cond
      (and (= t \L) (zero? (count-occupied adj))) \#
      (and (= t \#) (>= (count-occupied adj) 4)) \L
      :else t)))

(defn- update-tile-2
  "Tile update function for part 1 of the problem: a seat is filled if no
  seeable seats are occupied, and emptied if five or more seeable seats are
  occupied."
  [i j lyt]
  (let [t (get-in lyt [i j])
        adj (seeable-seats i j lyt)]
    (cond
      (and (= t \L) (zero? (count-occupied adj))) \#
      (and (= t \#) (>= (count-occupied adj) 5)) \L
      :else t)))

(defn- reseat
  "Apply seat occupation rules specified by upfn to lyt. upfn should take three
  arguments - row no., col no., and lyt, and return the new state of the seat in
  lyt at that position."
  [upfn lyt]
  (mapv-indexed
   (fn [i row] (mapv-indexed (fn [j t] (upfn i j lyt)) row))
   lyt))

(def ^:private layout
  "Seat layout on ferry."
  (->> "resources/day11.txt"
       slurp
       str/split-lines
       (mapv vec)))

(defn run
  []
  (letfn [(solve [upfn] (count-occupied
                         (equilibrium
                          (partial reseat upfn)
                          layout)))]
    (println "DAY 11")
    (println "PART 1")
    (println "The number of seats occupied stabilises to:")
    (println (solve update-tile-1))
    (println)
    (println "PART 2")
    (println "The number of seats occupied stabilises to:")
    (println (solve update-tile-2))))

(ns advent-2020.day10
  (:require [clojure.string :as str]))

(def ^:private joltages
  "Joltages of adapters, seat, and device in ascending order"
  (let [adapters (map #(Integer/parseInt %)
                      (str/split-lines (slurp "resources/day10.txt")))]
    ;; add 0 (seat), max + 3 (device), and sort
    (sort (into adapters [0 (+ (apply max adapters) 3)]))))

(defn- joltage-differences
  "For joltages js, return a seq of the joltage differences in corresponding
  order"
  ;; TODO: use eduction / create a reusable xform?
  [js]
  (->> js
       (partition 2 1)
       (map (comp - (partial apply -)))))

(defn- joltage-differences-freq
  "For joltages js, return a map of {diff freq} elements, where diff is the
  joltage difference of adjacent joltages, and freq is the frequency of that
  difference in js."
  [js]
  (frequencies (joltage-differences joltages)))

(defn- n-adapter-orders
  "Count the number of ways of ordering adapters with joltages js"
  [js]
  (letfn [(impl [mem-impl js j]
            (cond
              (zero? j) 1
              (contains? js j)
              (+ (mem-impl mem-impl js (dec j))
                 (mem-impl mem-impl js (- j 2))
                 (mem-impl mem-impl js (- j 3)))
              :else 0))]
    ;; wrap impl to ensure js is a set and allow scoped cache
    (impl (memoize impl) (set js) (apply max js))))

(defn run
  []
  (let [jdiffs (joltage-differences-freq joltages)]
    (println "DAY 10")
    (println "PART 1")
    (println "The 1-jolt differences × 3-jolt differences is:")
    (println (* (jdiffs 3) (jdiffs 1)))
    (println)
    (println "PART 2")
    (println "The number of ways of orderting the adapters is:")
    (println (n-adapter-orders joltages))))

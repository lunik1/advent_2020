(ns advent-2020.day12
  (:require [clojure.math.numeric-tower :refer [abs]]
            [clojure.set :refer [map-invert]]
            [clojure.string :as str]))

(def ^:private map-action
  {\N :north
   \W :west
   \E :east
   \S :south
   \F :forward
   \R :turn
   \L :turn})

(defn- parse-instruction
  "Parse instruction from input into map with keys :action, :val."
  [in]
  (let [[act val] (split-at 1 in)
        act (first act)
        val (Integer/parseUnsignedInt (str/join val))]
    (case act
      \R {:act :turn :val val}
      \L {:act :turn :val (- val)}
      {:act (map-action act) :val val})))

(def ^:private instructions
  "Parsed instructions from input"
  (->> "resources/day12.txt"
       slurp
       str/split-lines
       (map parse-instruction)))

(defn- turn
  "Direction faced after turning deg degrees widdershins if initially facing
  head. Heat must be one of :north, :west, :south, :east; and deg must be a
  multiple of 90. deg can be negative, representing a clockwise rotation."
  [head deg]
  (let [head->deg {:north 0 :east 90 :south 180 :west 270}
        deg->head (map-invert head->deg)]
    (-> head
        head->deg
        (+ deg)
        (mod 360)
        deg->head)))

(defn- rotate-waypoint
  "New location of waypoint at [x y] relative to ship, after rotation by deg
  degrees widdershins. deg must be multiple of 90 and may be negative."
  [[x y] deg]
  (case (mod deg 360)
    0 [x y]
    90 [y (- x)]
    180 [(- x) (- y)]
    270 [(- y) x]))

(defn- move
  "New position after heading dist steps in head direction, starting at [x y].
  head should be :north, :south, :east, or :west."
  [[x y] head dist]
  (case head
    :north [x (+ y dist)]
    :south [x (- y dist)]
    :east [(+ x dist) y]
    :west [(- x dist) y]))

(defn- move-to-waypoint
  "Move from pos to waypoint at wpos n times. Return final location [x y]."
  [pos wpos n]
  (mapv + pos (mapv (partial * n) wpos)))

(defn- set-sail
  "Sat sail using the part one rules with initial state ship and following
  instructions ins"
  [ship ins]
  (:pos
   (reduce
    (fn [ship {:keys [act val]}]
      (condp contains? act
        #{:north :east :south :west}
        (update ship :pos move act val)
        #{:forward}
        (update ship :pos move (:dir ship) val)
        #{:turn}
        (update ship :dir turn val)))
    ship
    ins)))

(defn- set-sail-waypoint
  "Sat sail using the part two rules with initial state ship and following
  instructions ins"
  [ship ins]
  (:pos
   (reduce
    (fn [ship {:keys [act val]}]
      (condp contains? act
        #{:north :east :south :west}
        (update ship :wpt move act val)
        #{:forward}
        (update ship :pos move-to-waypoint (:wpt ship) val)
        #{:turn}
        (update ship :wpt rotate-waypoint val)))
    ship
    ins)))

(defn- manhattan
  "Manhattan (taxicab) distance between [x1 y1] and [x2 y2]"
  [[x1 y1] [x2 y2]]
  (+ (abs (- x1 x2)) (abs (- y1 y2))))

(defn run
  []
  (let [ship {:pos [0 0] :dir :east :wpt [10 1]}]
    (println "DAY 12")
    (println "PART 1")
    (println "The manhattan distance of the final location is:")
    (println (manhattan [0 0] (set-sail ship instructions)))
    (println)
    (println "PART 2")
    (println "The manhattan distance of the final location is:")
    (println (manhattan [0 0] (set-sail-waypoint ship instructions)))))

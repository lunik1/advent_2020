(ns advent-2020.day17
  (:require [clojure.string :as str]))

;;;; Input parsing

(def ^:private input
  (io!
   (slurp "resources/day17.txt")))

(def ^:private active-cubes-3d
  "Set of initially active cubes"
  (:active
   (reduce
    (fn [{:keys [row col] :as state} ch]
      (case ch
        \. (update state :col inc)
        \# (-> state (update :col inc) (update :active #(conj % [row col 0])))
        \newline (-> state (update :row inc) (assoc :col 0))))
    {:row 0 :col 0 :active #{}}
    input)))

(def ^:private active-cubes-4d
  (set (map #(conj % 0) active-cubes-3d)))

;;;; Utility and common logic

(defn- cycle-impl
  "Run game of life cycle with neighbour function `nf`."
  [active nf]
  (set (for [[loc an] (frequencies (mapcat nf active))
             :when (or (= an 3) (and (= an 2) (contains? active loc)))]
         loc)))

;;;; 3D game of life

(defn- moore-3d
  "Return the co-ordinates in the 3D moore neighbourhood of [x y z]."
  [[x y z]]
  (for [i [-1 0 1]
        j [-1 0 1]
        k [-1 0 1]
        :when (not= [i j k] [0 0 0])]
    [(+ i x) (+ j y) (+ k z)]))

(defn- cycle-3d
  "Returns a set of the locations of active cubes as [x y z] tuples in the next
  cycle. `active` is a set of [x y z] tuples of active cubes in the current
  cycle."
  [active]
  (cycle-impl active moore-3d))

;;;; 4D game of life

(defn- moore-4d
  "Return the co-ordinates in the 4D moore neighbourhood of [x y z w]."
  [[x y z w]]
  (for [i [-1 0 1]
        j [-1 0 1]
        k [-1 0 1]
        l [-1 0 1]
        :when (not= [i j k l] [0 0 0 0])]
    [(+ i x) (+ j y) (+ k z) (+ l w)]))

(defn- cycle-4d
  "Returns a set of the locations of active cubes as [x y z w] tuples in the
  next cycle. `active` is a set of [x y z w] tuples of active cubes in the
  current cycle."
  [active]
  (cycle-impl active moore-4d))

;;;; Entrypoint

(defn run
  []
  (println "DAY 17")
  (println "PART 1")
  (println "The number of active cubes after six cycles is:")
  (println (count (nth (iterate cycle-3d active-cubes-3d) 6)))
  (println)
  (println "PART 2")
  (println "The number of active cubes after six cycles is:")
  (println (count (nth (iterate cycle-4d active-cubes-4d) 6))))
